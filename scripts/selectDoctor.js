import { visitForm } from "./modalAdd.js";
import {
  sendDentistCard,
  sendTherapistCard,
  sendCardiologistCard,
  cardId,
} from "./sendCard.js";
import { VisitDentist, VisitCardiologist, VisitTherapist } from "./app.js";

export function selectDoc() {
  let selectDoctors;
  selectDoctors = document.querySelector(".docror-selector");
  const doctorFormsgrup = document.createElement("div");
  doctorFormsgrup.classList.add("form-floating");
  doctorFormsgrup.classList.add("mb-3");

  selectDoctors.addEventListener("change", () => {
    console.log(selectDoctors.value);
    if (selectDoctors.value === "choose") {
      doctorFormsgrup.innerHTML = "";
    } else if (selectDoctors.value === "therapist") {
      doctorFormsgrup.innerHTML = "";
      const ageWraper = document.createElement("div");
      ageWraper.classList.add("form-floating");
      ageWraper.classList.add("mb-3");

      const age = document.createElement("input");
      age.type = "number";
      age.name = "age";
      age.placeholder = "Який ваш вік?";
      age.classList.add("form-control");
      age.classList.add("rounded-3");
      age.id = "floatingAge";

      const ageLable = document.createElement("label");
      ageLable.setAttribute("for", "floatingAge");
      ageLable.innerText = "Який ваш вік?";

      const btn = document.createElement("button");

      btn.type = "submit";
      btn.innerText = "submit";
      btn.classList.add("btn");
      btn.classList.add("btn-primary");

      const validData = document.createElement("p");
      validData.textContent = "Заповніть усі поля";
      validData.classList.add("hide");
      validData.classList.add("validData");
      visitForm.append(doctorFormsgrup);
      doctorFormsgrup.append(ageWraper);
      ageWraper.append(age);
      ageWraper.append(ageLable);

      doctorFormsgrup.append(btn, validData);
      btn.addEventListener("click", (e) => {
        e.preventDefault();
        //---------------------------------------
        const formForm = document.forms.sendForm;
        let id = localStorage.getItem("changeId")
          ? +localStorage.getItem("changeId")
          : cardId;
        //---------------------------------------
        if (
          formForm.fname.value === "" ||
          selectDoctors.value === "" ||
          formForm.title.value === "" ||
          formForm.typeVisit.value === "" ||
          formForm.age.value === "" ||
          formForm.visitStatus.value === ""
        ) {
          validData.classList.remove("hide");
          return;
        }
        sendTherapistCard();
      });
    } else if (selectDoctors.value === "dentist") {
      doctorFormsgrup.innerHTML = "";
      const dateWraper = document.createElement("div");
      dateWraper.classList.add("form-floating");
      dateWraper.classList.add("mb-3");

      const lastVisit = document.createElement("input");
      const btn = document.createElement("button");
      lastVisit.type = "date";
      lastVisit.name = "lastVisit";
      lastVisit.classList.add("form-control");
      lastVisit.classList.add("rounded-3");
      lastVisit.classList.add("mb-3");

      const lastVisitLable = document.createElement("label");
      lastVisitLable.setAttribute("for", "floatingDescription");
      lastVisitLable.innerText = "Дата вашого останього візиту";

      btn.type = "submit";
      btn.innerText = "submit";
      btn.classList.add("btn");
      btn.classList.add("btn-primary");
      const validData = document.createElement("p");
      validData.textContent = "Заповніть усі поля";
      validData.classList.add("hide");
      validData.classList.add("validData");

      visitForm.append(doctorFormsgrup);
      doctorFormsgrup.append(dateWraper);
      dateWraper.append(lastVisit);
      dateWraper.append(lastVisitLable);

      doctorFormsgrup.append(btn, validData);
      btn.addEventListener("click", (e) => {
        e.preventDefault();
        //---------------------------------------
        const formForm = document.forms.sendForm;
        let id = localStorage.getItem("changeId")
          ? +localStorage.getItem("changeId")
          : cardId;
        //---------------------------------------
        if (
          formForm.fname.value === "" ||
          selectDoctors.value === "" ||
          formForm.title.value === "" ||
          formForm.typeVisit.value === "" ||
          formForm.lastVisit.value === "" ||
          formForm.visitStatus === ""
        ) {
          validData.classList.remove("hide");
          return;
        }
        sendDentistCard();
      });
    } else if (selectDoctors.value === "cardiologist") {
      console.log("cardio");
      doctorFormsgrup.innerHTML = "";
      const btn = document.createElement("button");
      btn.type = "submit";
      btn.innerText = "submit";
      btn.classList.add("btn");
      btn.classList.add("btn-primary");
      const validData = document.createElement("p");
      validData.textContent = "Заповніть усі поля";
      validData.classList.add("hide");
      validData.classList.add("validData");

      const bpWraper = document.createElement("div");
      bpWraper.classList.add("form-floating");
      bpWraper.classList.add("mb-3");

      const bp = document.createElement("input");
      bp.type = "text";
      bp.name = "bp";
      bp.placeholder = "Який виш тиск?";
      bp.classList.add("form-control");
      bp.classList.add("rounded-3");
      bp.id = "floatingBp";

      const bpLable = document.createElement("label");
      bpLable.setAttribute("for", "floatingDescription");
      bpLable.innerText = "Який виш тиск?";

      const ageWraper = document.createElement("div");
      ageWraper.classList.add("form-floating");
      ageWraper.classList.add("mb-3");

      const age = document.createElement("input");
      age.type = "text";
      age.name = "age";
      age.placeholder = "Який ваш вік?";
      age.classList.add("form-control");
      age.classList.add("rounded-3");
      age.id = "floatingAge";

      const ageLable = document.createElement("label");
      ageLable.setAttribute("for", "floatingAge");
      ageLable.innerText = "Який ваш вік?";

      const weightWraper = document.createElement("div");
      weightWraper.classList.add("form-floating");
      weightWraper.classList.add("mb-3");

      const weight = document.createElement("input");
      weight.type = "number";
      weight.name = "weight";
      weight.placeholder = "Яка ваша вага?";
      weight.classList.add("form-control");
      weight.classList.add("rounded-3");
      weight.id = "floatingWeight";

      const weightLable = document.createElement("label");
      weightLable.setAttribute("for", "floatingAge");
      weightLable.innerText = "Яка ваша вага?";

      const pastDiseasesWraper = document.createElement("div");
      pastDiseasesWraper.classList.add("form-floating");
      pastDiseasesWraper.classList.add("mb-3");

      const pastDiseases = document.createElement("input");
      pastDiseases.type = "text";
      pastDiseases.name = "pastDiseases";
      pastDiseases.placeholder = "Якй ваш попередній діагноз?";
      pastDiseases.classList.add("form-control");
      pastDiseases.classList.add("rounded-3");
      pastDiseases.id = "floatinPpastDiseases";

      const pastDiseasesLable = document.createElement("label");
      pastDiseasesLable.setAttribute("for", "floatinPpastDiseases");
      pastDiseasesLable.innerText = "Якй ваш попередній діагноз?";

      visitForm.append(doctorFormsgrup);
      doctorFormsgrup.append(bpWraper);
      doctorFormsgrup.append(ageWraper);
      doctorFormsgrup.append(weightWraper);
      doctorFormsgrup.append(pastDiseasesWraper);
      bpWraper.append(bp);
      bpWraper.append(bpLable);
      ageWraper.append(age);
      ageWraper.append(ageLable);
      weightWraper.append(weight);
      weightWraper.append(weightLable);
      pastDiseasesWraper.append(pastDiseases);
      pastDiseasesWraper.append(pastDiseasesLable);
      doctorFormsgrup.append(btn, validData);
      btn.addEventListener("click", (e) => {
        e.preventDefault();
        //---------------------------------------
        const formForm = document.forms.sendForm;
        let id = localStorage.getItem("changeId")
          ? +localStorage.getItem("changeId")
          : cardId;
        //---------------------------------------
        if (
          formForm.fname.value === "" ||
          selectDoctors.value === "" ||
          formForm.title.value === "" ||
          formForm.typeVisit.value === "" ||
          formForm.bp.value === "" ||
          formForm.age.value === "" ||
          formForm.weight.value === "" ||
          formForm.pastDiseases.value === "" ||
          formForm.visitStatus.value === ""
        ) {
          validData.classList.remove("hide");
          return;
        }

        console.log(selectDoctors.value);

        sendCardiologistCard();
      });
    }
  });
}
