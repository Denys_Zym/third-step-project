import { selectDoc } from "./selectDoctor.js";

let newCardModal;
export let visitForm;

const btnCreateCard = document.querySelector(".btn-create-card");

btnCreateCard.addEventListener("click", () => {
  const newCard = new Modal("", "", "", "", "", "");
  newCard.createCard();
});
export class Modal {

  constructor(
    fullName,
    doctor,
    title,
    description,
    visitType,
    status,
    idCard,
    type
  ) {
    (this.fullName = fullName),
      (this.doctor = doctor),
      (this.title = title),
      (this.description = description),
      (this.visitType = visitType),
      (this.status = status),
      (this.idCard = idCard),
      (this.type = type);
  }

  createCard() {
    newCardModal = document.createElement("div");
    newCardModal.classList.add("newCardModal");
    //   d-block bg-secondary py-5
    newCardModal.classList.add("text-center");
    newCardModal.classList.add("modal");
    // newCardModal.classList.add('modal-signin')
    newCardModal.classList.add("position-absolut");
    newCardModal.classList.add("d-block");
    newCardModal.classList.add("bg-secondary");
    // newCardModal.classList.add('py-5')//пошукати змінити
    newCardModal.tabIndex = "-1";
    newCardModal.role = "dialog";
    // newCardModal.id = 'modalSignin'
    newCardModal.style.height = `100vh`;
    // newCardModal.classList.add('fade')
    // newCardModal.classList.add('show')
    // newCardModal.classList.add('offcanvas-backdrop')

    newCardModal.addEventListener("click", (e) => {
      e.target === newCardModal ? newCardModal.remove() : null;
    });

    const modalDialog = document.createElement("div");
    modalDialog.classList.add("modal-dialog");
    modalDialog.role = "document";

    const modalContent = document.createElement("div");
    modalContent.classList.add("modal-content");
    modalContent.classList.add("rounded-4");
    modalContent.classList.add("shadow");

    const modalHeder = document.createElement("div");
    modalHeder.classList.add("modal-header");
    modalHeder.classList.add("p-2");
    modalHeder.classList.add("pb-2");
    modalHeder.classList.add("border-bottom-0");

    const modalHed = document.createElement("h1");
    modalHed.classList.add("fw-bold");
    modalHed.classList.add("mb-0");
    modalHed.classList.add("fs-2");

    let resultText;
    if (this.type === "change") {
      localStorage.setItem("changeId", this.idCard);
      resultText = "Редактировать";
    } else {
      localStorage.setItem("changeId", "");
      resultText = "Створити візит";
    }
    modalHed.innerText = resultText;

    const btnClose = document.createElement("button");
    btnClose.type = "button";
    btnClose.classList.add("btn-close");
    btnClose.classList.add("btn");
    btnClose.setAttribute("aria-label", "Close");

    btnClose.addEventListener("click", () => {
      const newCardModal = document.querySelector(".newCardModal");
      newCardModal.remove();
    });

    const modalBody = document.createElement("div");
    modalBody.classList.add("modal-body");
    modalBody.classList.add("p-4"); //
    modalBody.classList.add("pt-0");

    visitForm = document.createElement("form");
    visitForm.name = "sendForm";
    visitForm.classList.add("new-form");

    const inputWraperName = document.createElement("div");
    inputWraperName.classList.add("form-floating");
    inputWraperName.classList.add("mb-3");

    const name = document.createElement("input");
    name.type = "text";
    name.name = "fname";
    name.value = this.fullName; ////////////////
    name.classList.add("form-control");
    name.classList.add("rounded-3");
    name.classList.add("text-black");
    name.placeholder = "Прізвище Ім'я по батькові";
    name.id = "floatingName";
    name.setAttribute("required", "");
    // name.required = true;

    const nameLable = document.createElement("label");
    nameLable.setAttribute("for", "floatingName");
    nameLable.innerText = "Прізвище Ім'я по батькові";

    const inputWraperTitle = document.createElement("div");
    inputWraperTitle.classList.add("form-floating");
    inputWraperTitle.classList.add("mb-3");

    const title = document.createElement("input");
    title.type = "text";
    title.name = "title";
    title.value = this.title; //////////////////////////////
    title.classList.add("form-control");
    title.classList.add("rounded-3");
    title.placeholder = "Мета візиту";
    title.id = "floatingTitle";

    const titleLable = document.createElement("label");
    titleLable.setAttribute("for", "floatingTitle");
    titleLable.innerText = "Мета візиту";

    const inputWraperDescription = document.createElement("div");
    inputWraperDescription.classList.add("form-floating");
    inputWraperDescription.classList.add("mb-3");

    const description = document.createElement("input");
    description.type = "text";
    description.name = "description";
    description.value = this.description; ////////////////////////////
    description.classList.add("form-control");
    description.classList.add("rounded-3");
    description.placeholder = "Коротко опишіть мету візиту";
    description.id = "floatingDescription";

    const descriptionLable = document.createElement("label");
    descriptionLable.setAttribute("for", "floatingDescription");
    descriptionLable.innerText = "Коротко опишіть мету візиту";

    const radioWraper = document.createElement("div");
    radioWraper.classList.add("form-check");
    radioWraper.classList.add("d-flex");
    radioWraper.classList.add("justify-content-evenly");

    const lableHigh = document.createElement("label");
    lableHigh.innerText = "High";
    lableHigh.classList.add("form-check-label");
    const high = document.createElement("input");
    high.type = "radio";
    high.name = "typeVisit";
    high.value = "high";
    // high.classList.add('form-check-input')

    const lableNormal = document.createElement("label");
    lableNormal.innerText = "Normal";
    lableNormal.classList.add("form-check-label");
    const normal = document.createElement("input");
    normal.type = "radio";
    normal.name = "typeVisit";
    normal.value = "normal";
    // normal.classList.add('form-check-input')

    const lableLow = document.createElement("label");
    lableLow.innerText = "low";
    lableLow.classList.add("form-check-label");
    const low = document.createElement("input");
    low.type = "radio";
    low.name = "typeVisit";
    low.value = "low";
    // low.classList.add('form-check-input')

    const selectDoctorWraper = document.createElement("div");
    selectDoctorWraper.classList.add("col-md-5");

    const selectDoctor = document.createElement("select");
    selectDoctor.classList.add("docror-selector");
    selectDoctor.classList.add("form-select");

    const selectionDoctor = document.createElement("option");
    selectionDoctor.innerText = "Виберіть лікаря";
    selectionDoctor.toggleAttribute("selected", "selected");
    selectionDoctor.value = "choose";

    const dentist = document.createElement("option");
    dentist.innerText = "Дантист";
    dentist.value = "dentist";

    const cardiologist = document.createElement("option");
    cardiologist.innerText = "Кардіолог";
    cardiologist.value = "cardiologist";
    cardiologist.classList.add("cardiologist");

    const therapist = document.createElement("option");
    therapist.innerText = "Терапевт";
    therapist.classList.add("therapist");
    therapist.value = "therapist";

    const statusWraper = document.createElement("div");
    statusWraper.classList.add("my-3");
    statusWraper.classList.add("form-check");
    statusWraper.classList.add("d-flex");
    statusWraper.classList.add("justify-content-evenly");

    const formCheckNotDon = document.createElement("div");
    formCheckNotDon.classList.add("form-check");
    const lableStatusNotDon = document.createElement("lable");
    lableStatusNotDon.innerText = "not don";
    lableStatusNotDon.classList.add("form-check-label");
    const notDon = document.createElement("input");
    notDon.type = "radio";
    notDon.name = "visitStatus";
    notDon.value = "open";
    notDon.checked = true;
    // notDon.classList.add('form-check-input')

    const formCheckDon = document.createElement("div");
    formCheckDon.classList.add("form-check");
    const lableStatusDon = document.createElement("lable");
    lableStatusDon.innerText = "done";
    lableStatusDon.classList.add("form-check-label");
    const don = document.createElement("input");
    don.type = "radio";
    don.name = "visitStatus";
    don.value = "done";
    // don.classList.add('form-check-input')
    ////
    console.log(
      name.value,
      title.value,
      radioWraper.value,
      selectDoctor.value,
      notDon.value
    );
    document.body.append(newCardModal);
    newCardModal.append(modalDialog);
    modalDialog.append(modalContent);
    modalContent.append(modalHeder);
    modalHeder.append(modalHed);
    modalHeder.append(btnClose);
    modalContent.append(modalBody);
    modalBody.append(visitForm);
    visitForm.append(inputWraperName);
    inputWraperName.append(name);
    inputWraperName.append(nameLable);

    visitForm.append(inputWraperTitle);
    inputWraperTitle.append(title);
    inputWraperTitle.append(titleLable);

    visitForm.append(inputWraperDescription);
    inputWraperDescription.append(description);
    inputWraperDescription.append(descriptionLable);

    visitForm.append(radioWraper);
    radioWraper.append(lableHigh);
    lableHigh.append(high);
    radioWraper.append(lableNormal);
    lableNormal.append(normal);
    radioWraper.append(lableLow);
    lableLow.append(low);

    visitForm.append(selectDoctorWraper);
    selectDoctorWraper.append(selectDoctor);
    selectDoctor.append(selectionDoctor);

    selectDoctor.append(dentist);
    selectDoctor.append(cardiologist);
    selectDoctor.append(therapist);
    visitForm.append(statusWraper);

    statusWraper.append(formCheckNotDon);
    formCheckNotDon.append(lableStatusNotDon);
    formCheckNotDon.append(notDon);

    statusWraper.append(formCheckDon);
    formCheckDon.append(lableStatusDon);
    lableStatusDon.append(don);
    selectDoc();
  }
}
