import { visits } from "./filter.js";
import { token } from "./login.js";
import { checkPosts } from "./checkPosts.js";

import { VisitDentist, VisitCardiologist, VisitTherapist } from "./app.js";
import {deleteCardVisits} from './filter.js'



export let cardId;

export function sendDentistCard() {

    const forma = document.forms.sendForm
    const changeId = localStorage.getItem("changeId");
    const selectDoctors = document.querySelector('.form-select')
    fetch("https://ajax.test-danit.com/api/v2/cards/" + changeId, {
      method: changeId ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        fullName: `${forma.fname.value}`,
        doctor: `${selectDoctors.value}`,
        title: `${forma.title.value}`,
        description: `${forma.description.value}`,
        visitType: `${forma.typeVisit.value}`,
        lastVisit: `${forma.lastVisit.value}`,
        status: `${forma.visitStatus.value}`,
      }),
      
    })
    
    .then((response) => response.json())
    
    .then(response => {cardId = response.id
      deleteCardVisits(cardId)
      visits.push(response)
      })
    .then(()=>{
      
      
      let allCards = document.querySelectorAll(".col");
      allCards.forEach((card) => {
        if (cardId === Number(card.getAttribute("id"))) {
          
          
          card.remove();
          
          
        }
      });
    })
    .then(() => {
        const formForm = document.forms.sendForm;
        const dentist = new VisitDentist(
            formForm.fname.value,
            selectDoctors.value,
            formForm.title.value,
            formForm.description.value,
            formForm.typeVisit.value,
            formForm.lastVisit.value,
            formForm.visitStatus,
            cardId
          );
          
          dentist.renderDentistCard();
        
    })
    
    .then(()=>
      {const removeModal = document.querySelector('.modal')
      removeModal.remove()
      checkPosts()}
      
    )
    .catch((error) => console.log("Error", error))

}

export function sendTherapistCard() {
  const forma = document.forms.sendForm;
  const changeId = localStorage.getItem("changeId");

  const selectDoctors = document.querySelector('.form-select')

  fetch("https://ajax.test-danit.com/api/v2/cards/" + changeId, {
    method: changeId ? "PUT" : "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      fullName: `${forma.fname.value}`,
      doctor: `${selectDoctors.value}`,
      title: `${forma.title.value}`,
      description: `${forma.description.value}`,
      visitType: `${forma.typeVisit.value}`,
      age: `${forma.age.value}`,
      status: `${forma.visitStatus.value}`,
    }),
  })
  .then((response) => response.json())
    
    .then(response => {cardId = response.id
      deleteCardVisits(cardId)
      visits.push(response)
      })
    .then(()=>{
      
      
      let allCards = document.querySelectorAll(".col");
      allCards.forEach((card) => {
        if (cardId === Number(card.getAttribute("id"))) {
          
          
          card.remove();
          
          
        }
      });
    })
    .then(() => {
        const formForm = document.forms.sendForm;
        const therapist = new VisitTherapist(
          formForm.fname.value,
          selectDoctors.value,
          formForm.title.value,
          formForm.description.value,
          formForm.typeVisit.value,
          formForm.age.value,
          formForm.visitStatus.value,
          cardId,
          );
          
          therapist.renderTherapistCard();
        
    })
    
    .then(()=>
      {const removeModal = document.querySelector('.modal')
      removeModal.remove()
      checkPosts()}
      
    )
    .catch((error) => console.log("Error", error))
  
}

export function sendCardiologistCard() {
  const forma = document.forms.sendForm;
  const changeId = localStorage.getItem("changeId");

  const selectDoctors = document.querySelector('.form-select')

  fetch("https://ajax.test-danit.com/api/v2/cards/" + changeId, {
    method: changeId ? "PUT" : "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      fullName: `${forma.fname.value}`,
      doctor: `${selectDoctors.value}`,
      title: `${forma.title.value}`,
      description: `${forma.description.value}`,
      visitType: `${forma.typeVisit.value}`,
      age: `${forma.age.value}`,
      bp: `${forma.bp.value}`,
      weight: `${forma.weight.value}`,
      pastDiseases: `${forma.pastDiseases.value}`,
      status: `${forma.visitStatus.value}`,
    }),
  })
    .then((response) => response.json())
      
    .then(response => {cardId = response.id
      deleteCardVisits(cardId)
      visits.push(response)
      })
    .then((response)=>{
      
      
      let allCards = document.querySelectorAll(".col");
      allCards.forEach((card) => {
        if (cardId === Number(card.getAttribute("id"))) {
          
          
          card.remove();
          
          
        }
      });
    })
    .then(() => {
        const formForm = document.forms.sendForm;
        const cardiologist = new VisitCardiologist(
          formForm.fname.value,
          selectDoctors.value,
          formForm.title.value,
          formForm.description.value,
          formForm.typeVisit.value,
          formForm.bp.value,
          formForm.age.value,
          formForm.weight.value,
          formForm.pastDiseases.value,
          formForm.visitStatus.value,
          cardId
          );
          
          cardiologist.renderCardiologistCard();
        
    })
    
    .then(()=>
      {const removeModal = document.querySelector('.modal')
      removeModal.remove()
      checkPosts()}
      
    )
    .catch((error) => console.log("Error", error))

}
