import { Modal } from "./modalAdd.js";
import { cardId } from "./sendCard.js";
import { token } from "./login.js";
import { visits } from "./filter.js";
import { checkPosts } from "./checkPosts.js";
import { deleteCardVisits } from "./filter.js";
let cardWraper;
let cardBody;

class Visit {
  constructor(fullName, doctor, title, description, visitType, status, cardId) {
    (this.fullName = fullName),
      (this.doctor = doctor),
      (this.title = title),
      (this.description = description),
      (this.visitType = visitType),
      (this.status = status),
      (this.cardId = cardId);
  }
  get renderCard() {
    const cardsWraper = document.querySelector(".cardsWraper");
    cardWraper = document.createElement("div");
    cardWraper.classList.add("col");

    cardBody = document.createElement("div");
    cardBody.classList.add("card-body");
    cardWraper.id = this.cardId;

    const cardBodyWraper = document.createElement("div");
    cardBodyWraper.classList.add("card");
    cardBodyWraper.classList.add("shadow-sm");

    const cardHeader = document.createElement("div");
    cardHeader.classList.add("d-flex");
    cardHeader.classList.add("py-3");
    cardHeader.classList.add("align-items-end");
    cardHeader.classList.add("flex-column");

    const ecranDoctor = document.createElement("p");
    ecranDoctor.classList.add("card-text");

    const ecranFullName = document.createElement("h3");
    ecranFullName.classList.add("card-text");

    ecranDoctor.innerText = this.doctor;
    ecranFullName.innerText = this.fullName;

    const btnRemove = document.createElement("button");
    btnRemove.type = "button";

    btnRemove.classList.add("btn");
    btnRemove.classList.add("btn-close");

    const btnGrup = document.createElement("div");
    btnGrup.classList.add("btn-group");

    const btnEdit = document.createElement("button");
    btnEdit.type = "button";
    btnEdit.innerText = "Редагувати";
    btnEdit.classList.add("btn");
    btnEdit.classList.add("btn-sm");
    btnEdit.classList.add("btn-outline-secondary");

    btnEdit.addEventListener("click", () => {
      console.log(this.visitType, this.status, this.idCard);
      const editCardModal = new Modal(
        this.fullName,
        this.doctor.value,
        this.title,
        this.description,
        this.visitType.value,
        this.status.value,
        this.cardId,
        "change"
      );
      editCardModal.createCard();

      //   console.log(visits.indexOf(visits{this.cardId}));
    });

    const morBtn = document.createElement("button");
    morBtn.type = "button";
    morBtn.innerText = "Показати більше";
    morBtn.classList.add("btn");
    morBtn.classList.add("btn-sm");
    morBtn.classList.add("btn-outline-secondary");
    morBtn.classList.add("morBtn");

    cardsWraper.append(cardWraper);

    cardWraper.append(cardBodyWraper);
    cardBodyWraper.append(cardHeader);
    cardBodyWraper.append(cardBody);

    cardBody.append(ecranFullName);
    cardBody.append(ecranDoctor);

    cardBody.insertAdjacentElement("afterend", btnGrup);
    cardHeader.append(btnRemove);
    btnGrup.append(btnEdit);
    btnGrup.append(morBtn);

    btnRemove.addEventListener("click", () => {
      this.deliteCard();
    });

    morBtn.addEventListener("click", () => {
      morBtn.disabled = true;
      this.moreInfo();
    });
  }

  moreInfo() {
    visits.filter((infoVisit) => {
      if (infoVisit.id === this.cardId) {
        console.log(infoVisit.id, this.cardId);
        const index = this.cardId;
        const addInfo = document.getElementById(index);
        const tar = addInfo.querySelector(".card-body");
        console.log(infoVisit);
        const ecranStatus = document.createElement("p");
        ecranStatus.classList.add("card-text");
        const ecranTitle = document.createElement("h2");
        ecranTitle.classList.add("card-text");
        const ecranDescription = document.createElement("p");
        ecranDescription.classList.add("card-text");
        const ecranVisitType = document.createElement("p");
        ecranVisitType.classList.add("card-text");
        ecranStatus.innerText = infoVisit.status;
        ecranTitle.innerText = infoVisit.title;
        ecranDescription.innerText = infoVisit.description;
        ecranVisitType.innerText = infoVisit.visitType;
        tar.append(ecranDescription);
        tar.append(ecranVisitType);
        tar.append(ecranStatus);
      }
    });
  }

  deliteCard() {
    console.log(this.cardId);
    fetch(`https://ajax.test-danit.com/api/v2/cards/${this.cardId}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => {
        if (response.status === 200) {
          let allCards = document.querySelectorAll(".col");
          allCards.forEach((card) => {
            if (this.cardId === Number(card.getAttribute("id"))) {
              card.remove();
            }
          });

          deleteCardVisits(this.cardId); //функция удаления елемента из массива
          checkPosts(); // функция показа текста если карточек нету
          console.log(visits);
        }
      })
      .catch((error) => console.log("Error", error));
  }
}

export class VisitDentist extends Visit {
  constructor(
    fullName,
    doctor,
    title,
    description,
    visitType,
    lastVisit,
    status,
    cardId
  ) {
    super(fullName, doctor, title, description, visitType, status, cardId);
    this.lastVisit = lastVisit;
  }

  renderDentistCard() {
    this.renderCard;
  }
  moreInfo() {
    visits.filter((infoVisit) => {
      if (infoVisit.id === this.cardId) {
        const index = this.cardId;
        const addInfo = document.getElementById(index);
        const tar = addInfo.querySelector(".card-body");

        const ecranLastVisit = document.createElement("p");
        ecranLastVisit.classList.add("card-text");
        const ecranTitle = document.createElement("h2");
        ecranTitle.classList.add("card-text");
        ecranTitle.innerText = infoVisit.title;

        const ecranDescription = document.createElement("p");
        ecranDescription.classList.add("card-text");
        const ecranStatus = document.createElement("p");
        ecranStatus.classList.add("card-text");
        const ecranVisitType = document.createElement("p");
        ecranVisitType.classList.add("card-text");
        ecranVisitType.innerText = infoVisit.visitType;

        ecranLastVisit.innerText = infoVisit.lastVisit;

        ecranDescription.innerText = infoVisit.description;
        ecranStatus.innerText = infoVisit.status;

        tar.prepend(ecranTitle);
        tar.append(ecranLastVisit);

        tar.append(ecranVisitType);
        tar.append(ecranStatus);
        tar.append(ecranDescription);
      }
    });
  }
}

export class VisitCardiologist extends Visit {
  constructor(
    fullName,
    doctor,
    title,
    description,
    visitType,
    bp,
    age,
    weight,
    pastDiseases,
    status,
    cardId
  ) {
    super(fullName, doctor, title, description, visitType, status, cardId);
    (this.bp = bp),
      (this.age = age),
      (this.weight = weight),
      (this.pastDiseases = pastDiseases);
  }
  renderCardiologistCard() {
    this.renderCard;
  }
  moreInfo() {
    visits.filter((infoVisit) => {
      if (infoVisit.id === this.cardId) {
        console.log(infoVisit.id);
        const index = this.cardId;
        const addInfo = document.getElementById(index);
        const tar = addInfo.querySelector(".card-body");
        const ecranBp = document.createElement("p");
        ecranBp.classList.add("card-text");
        const ecranAge = document.createElement("p");
        ecranAge.classList.add("card-text");
        const ecranWeight = document.createElement("p");
        ecranWeight.classList.add("card-text");
        const ecranPastDiseases = document.createElement("p");
        ecranPastDiseases.classList.add("card-text");
        const ecranDescription = document.createElement("p");
        ecranDescription.classList.add("card-text");
        const ecranStatus = document.createElement("p");
        ecranStatus.classList.add("card-text");
        const ecranVisitType = document.createElement("p");
        ecranVisitType.classList.add("card-text");
        ecranVisitType.innerText = infoVisit.visitType;
        ecranBp.innerText = infoVisit.bp;
        ecranAge.innerText = infoVisit.age;
        ecranWeight.innerText = infoVisit.weight;
        ecranPastDiseases.innerText = infoVisit.pastDiseases;
        ecranDescription.innerText = infoVisit.description;
        ecranStatus.innerText = infoVisit.status;
        const ecranTitle = document.createElement("h2");
        ecranTitle.classList.add("card-text");
        ecranTitle.innerText = infoVisit.title;

        tar.prepend(ecranTitle);
        tar.append(ecranBp);
        tar.append(ecranAge);
        tar.append(ecranWeight);
        tar.append(ecranPastDiseases);
        tar.append(ecranVisitType);
        tar.append(ecranStatus);
        tar.append(ecranDescription);
      }
    });
  }
}

export class VisitTherapist extends Visit {
  constructor(
    fullName,
    doctor,
    title,
    description,
    visitType,
    age,
    status,
    cardId
  ) {
    super(fullName, doctor, title, description, visitType, status, cardId);
    this.age = age;
  }
  renderTherapistCard() {
    this.renderCard;
  }
  moreInfo() {
    visits.filter((infoVisit) => {
      if (infoVisit.id === this.cardId) {
        console.log(infoVisit.id);
        const index = this.cardId;
        const addInfo = document.getElementById(index);
        const tar = addInfo.querySelector(".card-body");

        const ecranAge = document.createElement("p");
        ecranAge.classList.add("card-text");
        const ecranTitle = document.createElement("h2");
        ecranTitle.classList.add("card-text");
        ecranTitle.innerText = infoVisit.title;

        const ecranDescription = document.createElement("p");
        ecranDescription.classList.add("card-text");
        const ecranStatus = document.createElement("p");
        ecranStatus.classList.add("card-text");
        const ecranVisitType = document.createElement("p");
        ecranVisitType.classList.add("card-text");
        ecranVisitType.innerText = infoVisit.visitType;

        ecranAge.innerText = infoVisit.age;

        ecranDescription.innerText = infoVisit.description;
        ecranStatus.innerText = infoVisit.status;

        tar.prepend(ecranTitle);
        tar.append(ecranAge);

        tar.append(ecranVisitType);
        tar.append(ecranStatus);
        tar.append(ecranDescription);
      }
    });
  }
}
