import { VisitDentist, VisitCardiologist, VisitTherapist } from "./app.js";
// import { token } from "./sendCard.js";
import { token } from "./login.js";
import { checkPosts } from "./checkPosts.js";

export let visits;
let statusElem = "";
let visitTypeElem = "";
let inputElem = "";

// CREATE HTML FOR FILTER BLOCK
const filter = document.querySelector("#filter");
filter.innerHTML = ` 
<form class="filter__form" action="">
<div class="filter__item search">
  <img src="./img/search-1.png" waight="15" height="15" alt="" />
  <input id="title" name="title" placeholder="Search by tittle" autocomplete="off"/>
</div>
<select class="filter__item status" name="status" id="status">
  <option value="status" selected disabled hidden>Статус</option>
  <option value="open">Open</option>
  <option value="done">Done</option>
</select>
<select
  class="filter__item visit-type" name="visit-type" id="visit-type">
  <option value="default" selected disabled hidden>Терміновість візиту</option>
  <option value="high">High</option>
  <option value="normal">Normal</option>
  <option value="low">Low</option>
</select>
</form>
<button type="submit" id="search-btn">Search</button>`;

const searchBtn = document.querySelector("#search-btn");
searchBtn.addEventListener("click", showFilteredCards);

function showFilteredCards() {
  inputElem = document.querySelector("#title").value.toLowerCase();
  statusElem = document.querySelector("#status").value;
  visitTypeElem = document.querySelector("#visit-type").value;
  if (inputElem && statusElem && visitTypeElem) {
    renderFilteredVisit();
  }
}

// GET CARDS
function getCards() {
  fetch("https://ajax.test-danit.com/api/v2/cards", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then((r) => r.json())
    .then((data) => {
      visits = data;
      console.log("data:", data);
      visits.forEach((i) => {
        renderVisit(i);
      });
      checkPosts(); // функция показа текста если карточек нету
    })
    .catch((error) => console.log("Error", error));
}
export { getCards };

//RENDER VISITS IN HTML
function renderVisit(i) {
  if (i.doctor === "dentist") {
    // console.log(i);
    const cardDentist = new VisitDentist(
      i.fullName,
      i.doctor,
      i.title,
      i.description,
      i.visitType,
      i.lastVisit,
      i.status,
      i.id
    );
    cardDentist.renderDentistCard();
  }
  if (i.doctor === "cardiologist") {
    const cardCardiologist = new VisitCardiologist(
      i.fullName,
      i.doctor,
      i.title,
      i.description,
      i.visitType,
      i.bp,
      i.age,
      i.weight,
      i.pastDiseases,
      i.status,
      i.id
    );
    cardCardiologist.renderCardiologistCard();
  }
  if (i.doctor === "therapist") {
    const cardTherapist = new VisitTherapist(
      i.fullName,
      i.doctor,
      i.title,
      i.description,
      i.visitType,
      i.age,
      i.status,
      i.id
    );
    cardTherapist.renderTherapistCard();
  }
}

//FILTER OF VISITS
function renderFilteredVisit() {
  const listItem = document.querySelector(".cardsWraper");
  listItem.innerHTML = "";

  visits
    .filter((visit) => {
      return (
        visit.title?.toLowerCase().includes(inputElem) &&
        visit.status === statusElem &&
        visit.visitType === visitTypeElem
      );
    })
    .forEach((visit) => {
      renderVisit(visit);
    });
}

export function deleteCardVisits(cardId) {
  visits = visits.filter((obj) => obj.id !== cardId);
}
